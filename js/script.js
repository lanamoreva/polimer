$(function() {
	$(".go").click(function() {
		var link = $(this).attr("href");
		if ($(link).length != 0) {
			$("html, body").animate({
				scrollTop: $(link).offset().top - 90
			}, 800)
		}
		return !1
	});
	$('a[href=#ord]').on('click', function() {
		$('body').addClass('fix');
		$('.modal').fadeIn();
		let orderName = $(this).parents('div').find('#name p').eq(0).text();
		if (orderName.length) {
			$('.modal form input[name=ord]').val(orderName).show()
		}
	});
	$('.modal').click(function(e) {
		if(e.target.className == 'modal'){
			$('body').removeClass();
			$('.modal').fadeOut();
			$('.modal form input[name=ord]').attr('value', '').hide();
		}
	});
	var field = new Array("name","phone");
	$("form").submit(function() {
		var error = 0;
		$(this).find(":input").each(function() {
			for (var i = 0; i < field.length; i++) {
				if ($(this).attr("name") == field[i]) {
					if (!$(this).val()) {
						$(this).css("border-color", "red ");
						error = 1
					} else {
						$(this).css("border-color", "green")
					}
				}
			}
		});
		var phone = $(this).find("input[name=phone]").val();
		if (!validNumber(phone)) {
			error = 2;
			$(this).find("input[name=phone]").css("border-color", "red")
		}
		if (error !== 0) {
			return !1
		}
		function validNumber(g) {
			var h = new RegExp(/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/);
			return h.test(g)
		}
	});
	if ($('.sld').length) {
		var slSwiper = new Swiper('.sld', {
			slidesPerView: 2,
			spaceBetween: 15,
			slidesPerGroup: 3,
			loopedSlides: 31,
			loop: true,
			autoWidth: true,
			navigation: {
				nextEl: '.swiper-button-next',
				prevEl: '.swiper-button-prev',
			}
		});
	}
	
	if ($('.lazy').length) {
		$('.lazy').lazy({
			delay: 1
		});
	}
	if ($('#map').length) {
		ymaps.ready(init)
	}
	let myMap;
	function init(){
		var i, place, center;
		if ($(window).width() <= '768')
			center = [54.960381903814884,82.93551440674577];
		else
			center = [54.95904221714632,82.95033092934405];
		var pointer = [54.960149622648096,82.93556000429908];
		var myMap = new ymaps.Map("map",{
			center: center,
			zoom: 14,
			controls: ['zoomControl'],
		});
		place = new ymaps.Placemark(pointer,{
			hintContent: ''
		},{
			iconLayout: 'default#image',
			iconImageHref: '/img/marker.png',
			iconImageSize: [29, 40],
			iconImageOffset: [-12, -40]
		})
		myMap.geoObjects.add(place);
		myMap.behaviors.disable(['scrollZoom'])
	}
	if ($('.sld-main').length) {

	var autoSwiper = new Swiper('.sld-main', {
		spaceBetween: 0,
		centeredSlides: true,
		loop: true,
		autoplay: {
		  delay: 3500,
		  disableOnInteraction: false,
		},
		pagination: {
		  el: '.swiper-pagination',
		  clickable: true,
		},
		navigation: {
		  nextEl: '.swiper-button-next',
		  prevEl: '.swiper-button-prev',
		},
	  });
	}

	!function(d,s){
		var js,fjs=d.getElementsByTagName(s)[0];js=d.createElement(s);
		js.async=1;js.src="//widget.flamp.ru/loader.js";
		fjs.parentNode.insertBefore(js,fjs);
	}(document,"script")


	
})



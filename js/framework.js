// примеры работ
const examplesWork = [];

for(let i=0; i<31; i++) {
  const item = {
    order: i+1,
    img: `${i+1}.jpg`,
  }
  examplesWork.push(item);
}
// конец примеры работ

// главная слайд
const mainSl = [];
for(i=0;i<4;i++) {
	const item = {
		order: i+1,
		img: `${i+1}.jpg`,
	}
	mainSl.push(item);

}

// конец гл.слайд

// партнеры
const partners = [];
for(i=0;i<9;i++) {
	const item = {
		order: i+1,
		img: `${i+1}.png`,
	}
	partners.push(item);

}


new Vue({
	el: '#root',
	data: {
		examplesWork,
		mainSl,
		partners,
		isMenu: false,

	},
	methods: {
		showMenu(){
			this.isMenu = !this.isMenu;
		},

	}
})
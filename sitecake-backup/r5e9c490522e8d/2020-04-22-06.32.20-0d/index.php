<!DOCTYPE html>
<html>
    <head>
        <meta content="text/html; charset=utf-8" http-equiv="Content-Type">
        <meta content="index, follow" name="robots">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>
            Полимер — 
            <?php echo $_SERVER['HTTP_HOST'] ?>
            </title>
            <link href="favicon.ico" rel="shortcut icon" type="image/x-icon">
            <link href="css/style.css?<?php echo rand() ?>
            " rel="stylesheet" type="text/css"> 
            <link href="css/jquery.fancybox.min.css" rel="stylesheet" type="text/css">
            <link href="css/swiper.min.css" rel="stylesheet" type="text/css">
            <script src="js/jquery.min.js" type="text/javascript" defer></script>
            <script src="js/jquery.fancybox.min.js" type="text/javascript" defer></script>
            <script src="js/swiper.min.js" type="text/javascript" defer></script>
            <script src="https://api-maps.yandex.ru/2.1/?lang=ru-RU" type="text/javascript" defer></script>
            <script src="js/script.js" type="text/javascript" defer></script>
            <script src="js/vue.js" type="text/javascript"></script>
        </head>
        <body>
            <div id="root">
                <header>
                    <div class="fond">
                        <div class="hood">
                            <div class="wrap">
                                <div class="on">
                                    <div class="lf sc-content">
                                        <a href="#">
                                            <img src="img/logo.png">
                                        </a>
                                    </div>
                                    <div @click="showMenu" class="mob-burger">
                                        <img src="img/burger.jpg" alt="">
                                    </div>
                                    <div class="cn sc-content">
                                        <ul>
                                            <li> <a href="#main" class="go">Главная</a> </li>
                                            <li> <a href="#advantages" class="go">Наши приемущества</a> </li>
                                            <li> <a href="#on" class="go">Материалы</a> </li>
                                            <li> <a href="#tw" class="go">Примеры работ</a> </li>
                                            <li> <a href="#map" class="go">Контакты</a> </li>
                                        </ul>
                                    </div>
                                    <div class="rt">
                                        <a href="tel:+79139172791">
                                            <div class="r sc-content">
                                                <div class="icon-phone sc-content">
                                                    <img src="img/phone.png" alt="">
                                                </div>
                                                <p>8 (913) 917-27‒91</p>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- menu-mob -->
                        <div v-if="isMenu" @click="showMenu" class="overlay-menu" v-cloak></div>
                        <div class="header-mob-wrapper" :class="{'mob-menu-show': isMenu}">
                            <div class="mob-close-menu" @click="showMenu">
                                <img src="img/close-menu.svg">
                            </div>
                            <div class="header-mob">
                                <div class="logo-mob sc-content">
                                    <img class="img-header-mob" src="img/logo.png" alt="">
                                </div>
                                <div class="menu-mob-box sc-content">
                                    <ul class="menu-mob">
                                        <li><a href="#main" class="go">Главная</a></li>
                                        <li><a href="#advantages" class="go">Наши приемущества</a></li>
                                        <li><a href="#on" class="go">Материалы</a></li>
                                        <li><a href="#tw" class="go">Примеры работ</a></li>
                                        <li><a href="#map" class="go">Контакты</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <!-- menu-mob -->
                        <div class="head" id="main">
                            <div class="swiper-container sld-main">
                                <div class="swiper-wrapper">
                                    <div v-for="n in mainSl" :key="n.order" class="swiper-slide" v-bind:style="{background: `linear-gradient(122.75deg, rgba(41, 253, 240, 0.2) 4.5%, rgba(113, 64, 252, 0.2) 83.26%), url(/img/main/${n.img}) no-repeat center/cover`}"></div>
                                </div>
                                <!-- Add Pagination -->
                                <div class="swiper-pagination"></div>
                                <!-- Add Arrows -->
                                <div class="swiper-button-next"></div>
                                <div class="swiper-button-prev"></div>
                            </div>
                            <!-- <div class="mob-btn sc-content">
                            <a href="#form-mob" class="but go">Заказать</a> 
                        </div>
                        --> 
                        <div class="wrap">
                            <div class="on">
                                <div class="a sc-content">
                                    <h1>Пластмассовые и пластиковые изделия</h1>
                                    <p>Литье изделий из пластика</p>
                                    <p>Литье изделий из резины</p>
                                    <p>Термоформирование резиновых изделий</p>
                                    <p>3D-печать</p>
                                    <p>Полимерные материалы</p>
                                </div>
                                <div class="head-form">
                                    <!-- форму сюда -->
                                    <form action="order.php" method="post">
                                        <div class="form-wrap">
                                            <div class="form-wr sc-content">
                                                <h4>Контакты</h4>
                                                <div class="form-box sc-content">
                                                    <p>Имя</p>
                                                    <input type="text" name="name" placeholder="Имя">
                                                </div>
                                                <div class="form-box sc-content">
                                                    <p>Телефон для связи</p>
                                                    <input type="text" name="phone" placeholder="+7 (999) 999 99 99">
                                                </div>
                                                <!-- <input type="submit" value="&#1054;&#1090;&#1087;&#1088;&#1072;&#1074;&#1080;&#1090;&#1100; &#1079;&#1072;&#1103;&#1074;&#1082;&#1091;" class="but">
                                                <label>&#1054;&#1090;&#1087;&#1088;&#1072;&#1074;&#1083;&#1103;&#1103; &#1092;&#1086;&#1088;&#1084;&#1091;, &#1103; &#1076;&#1072;&#1102; &#1089;&#1074;&#1086;&#1077; &#1089;&#1086;&#1075;&#1083;&#1072;&#1089;&#1080;&#1077; &#1085;&#1072; &#1086;&#1073;&#1088;&#1072;&#1073;&#1086;&#1090;&#1082;&#1091; <a href="policy.php" target="_blank" data-fancybox data-type="ajax" data-filter=".policy" data-src="policy.php">&#1087;&#1077;&#1088;&#1089;&#1086;&#1085;&#1072;&#1083;&#1100;&#1085;&#1099;&#1093; &#1076;&#1072;&#1085;&#1085;&#1099;&#1093;</a></label>
                                                --> 
                                            </div>
                                            <div class="form-wr sc-content">
                                                <h4>Характеристики</h4>
                                                <!-- <div class="form-box sc-content">
                                                <p>&#1058;&#1080;&#1087; &#1087;&#1086;&#1074;&#1077;&#1088;&#1093;&#1085;&#1086;&#1089;&#1090;&#1080;</p>
                                                <select name="style" id="">
                                                    <option>&#1052;&#1072;&#1090;&#1086;&#1074;&#1072;&#1103;</option>
                                                    <option>&#1043;&#1083;&#1103;&#1085;&#1094;&#1077;&#1074;&#1072;&#1103;</option>
                                                </select>
                                            </div>
                                            --> 
                                            <div class="form-box sc-content">
                                                <p>Вид материала</p>
                                                <select name="type" id="">
                                                    <option>Полиуретановый пластик</option>
                                                    <option>Полиуретановая резина</option>
                                                    <option>Пенополиуретан</option>
                                                    <option>Силикон</option>
                                                </select>
                                            </div>
                                            <!-- <div class="box">
                                            --> 
                                            <div class="form-box sc-content">
                                                <p>Кол-во, шт</p>
                                                <input type="text" name="count">
                                            </div>
                                            <!-- <div class="form-box sc-content">
                                            <p>&#1042;&#1077;&#1089; &#1086;&#1076;&#1085;&#1086;&#1081;</p>
                                            <input type="text" name="weight">
                                        </div>
                                        --> 
                                        <!-- </div>
                                        --> 
                                        <!-- <div class="form-box sc-content">
                                        <p>&#1056;&#1072;&#1079;&#1084;&#1077;&#1088;&#1099; (&#1084;&#1084; &#1093; &#1084;&#1084; &#1093; &#1084;&#1084;)</p>
                                        <div class="box-three">
                                            <input type="text" name="size1">
                                            <span>x</span> 
                                            <input type="text" name="size2">
                                            <span>x</span> 
                                            <input type="text" name="size3">
                                        </div>
                                    </div>
                                    --> 
                                </div>
                            </div>
                            <div class="btn-form-box">
                                <input type="submit" value="Отправить заявку" class="but">
                                <label>Отправляя форму, я даю свое согласие на обработку <a href="policy.php" target="_blank" data-fancybox data-type="ajax" data-filter=".policy" data-src="policy.php">персональных данных</a></label>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="mob-btn sc-content"> <a href="#form-mob" class="but go">Заказать</a> </div>
            </div>
        </div>
    </div>
</header>
<section id="advantages" class="wrap">
    <div class="advantages-wrapper">
        <div class="advantages-box sc-content">
            <img src="img/time.jpg" alt="">
            <p>
                Быстрый 
                <br>
                расчет заказа 
            </p>
        </div>
        <div class="advantages-box sc-content">
            <img src="img/oborudovanie.jpg" alt="">
            <p>
                Современное 
                <br>
                оборудование 
            </p>
        </div>
        <div class="advantages-box sc-content">
            <img src="img/specialist.jpg" alt="">
            <p>
                Квалифицированные 
                <br>
                специалисты 
            </p>
        </div>
        <div class="advantages-box sc-content">
            <img src="img/10year.jpg" alt="">
            <p>
                Опыт 
                <br>
                работы 
            </p>
        </div>
        <div class="advantages-box sc-content">
            <img src="img/process.jpg" alt="">
            <p>
                Полный цикл 
                <br>
                производства 
            </p>
        </div>
        <div class="advantages-box sc-content">
            <img src="img/top.jpg" alt="">
            <p>
                Высокое 
                <br>
                качество 
            </p>
        </div>
    </div>
</section>
<section id="on">
    <div class="serv materials-wrapper">
        <div class="wrap">
            <div class="sc-content">
                <h3>Материалы для литья</h3>
            </div>
            <div class="on materials-wr">
                <div class="h materials-box">
                    <div id="name" class="b sc-content materials">
                        <p>Полиуретановый пластик</p>
                        <ul>
                            <li>Имитируют АБС, ПК (поликарбонат) и ПММА (акриловая смола)</li>
                            <li>Устойчивы к ультрафиолету</li>
                            <li>Выдерживают температуру до 105°С</li>
                            <li>Доступно литье из пластиков с толщинами до 5 мм, до 10 мм, до 50 мм</li>
                            <li>Пластики отличаются уровнем прозрачности при разных толщинах</li>
                            <li>Прозрачные полиуретаны поддаются колеровке, также возможно делать закладные и нарезать резьбу</li>
                        </ul>
                        <p class="btn-more"><a href="#ord">Оставить заявку</a></p>
                    </div>
                </div>
                <div class="h materials-box">
                    <div id="name" class="b sc-content materials">
                        <p>Пенополиуретан</p>
                        <ul>
                            <li>Имитируют АБС пластик</li>
                            <li>Стандартно льем детали из черного и белого пластика, возможно окрашивание в любой цвет по таблице RAL Classic</li>
                            <li>Материалы отличаются по прочности и термостойкости</li>
                            <li>Возможно литье из пластика с термостойкостью до 130°С</li>
                            <li>Отлитые изделия могут иметь матовую или глянцевую поверхность</li>
                            <li>Возможно делать закладные и нарезать резьбу</li>
                        </ul>
                        <p class="btn-more"><a href="#ord">Оставить заявку</a></p>
                    </div>
                </div>
                <div class="h materials-box">
                    <div id="name" class="b sc-content materials">
                        <p>Полиуретановая резина</p>
                        <ul>
                            <li>Сопоставимы с каучуком с различной твердостью по Шору А</li>
                            <li>Стандартно используем три вида материала с твердостью: 30 А, 40 А и 60 А</li>
                            <li>Возможно литье из резиноподобного материала с твердостью 80 А и 90 А</li>
                            <li>Стандартно льем черный полиуретан, возможно окрашивание по RAL Classic</li>
                            <li>Отлитые изделия могут иметь матовую или глянцевую поверхность</li>
                            <li>Возможно делать закладные</li>
                        </ul>
                        <p class="btn-more"><a href="#ord">Оставить заявку</a></p>
                    </div>
                </div>
                <div class="h materials-box">
                    <div id="name" class="b sc-content materials">
                        <p>Cиликон</p>
                        <ul>
                            <li>Долгий срок службы</li>
                            <li>Не поддерживает горение</li>
                            <li>Прочность на растяжение до 800%</li>
                            <li>Отличная электрическая изоляция</li>
                            <li>Высокая прочность и долговечность</li>
                            <li>Отсутствие запаха, вкуса, токсичности</li>
                            <li>Отличная термоморозостойкость от-100°С до +315°С</li>
                            <li>Устойчивы к давлению, излучению, сжатию, электромагнитному воздействию и агрессивным средам</li>
                            <li>Стандартно льем черный силикон, возможно окрашивание по RAL Classic</li>
                            <li>Отлитые изделия могут иметь матовую или глянцевую поверхность</li>
                        </ul>
                        <p class="btn-more"><a href="#ord">Оставить заявку</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="th">
    <div class="technologies">
        <div class="wrap">
            <div class="sc-content">
                <h3>Технологии:</h3>
            </div>
            <div class="on">
                <div class="l sc-content">
                    <p>Изготовление мягких форм из силикона или полиуретановой резины</p>
                </div>
                <div class="l sc-content">
                    <p>Литье пластика (полиуретана) в мягкие формы</p>
                </div>
                <div class="l sc-content">
                    <p>Изготовление стальных форм для изготовления изделий из резины и силикона методом термопресс формированием и изделий из пенополиуретана</p>
                </div>
                <div class="l sc-content">
                    <p>Термопрессформирование РТИ из каучуковых резин и силикона</p>
                </div>
                <div class="l sc-content">
                    <p>3D печать</p>
                    <ul>
                        <li>FDM - послойная укладка полимера. Использует пластики: ABS, PLA, PETG</li>
                        <li>SLS– селективное лазерное спекание. Полиамид</li>
                        <li>SLA технология — лазерная стереолитография. Фотополимеры</li>
                    </ul>
                </div>
                <!-- <div class=" sc-content"></div>
            --> 
        </div>
    </div>
</div>
</section>
<section id="tw">
<div class="work">
    <div class="wrap">
        <div class="sc-content">
            <h3>Примеры работ</h3>
        </div>
        <div class="swiper-container sld">
            <div class="swiper-wrapper">
                <div v-for="example in examplesWork" :key="example.order" class="on swiper-slide">
                    <img :src="`img/work/${example.img}`" class="swiper-lazy">
                </div>
            </div>
            <!-- Add Arrows -->
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
        </div>
    </div>
</div>
</section>
<section class="partner">
<div class="wrap">
    <div class="on">
        <div class="part-text sc-content">
            <h4>Нас уже выбрали</h4>
            <p>Мы ценим опыт и профессианализм партнеров и только по таким принципам формируем новые партнерские отношения.</p>
        </div>
        <div class="logo-partners">
            <div v-for="partner in partners" :key="partner.order" class="logo-pr sc-content">
                <a href="#">
                    <img :src="`img/partners/${partner.img}`" alt="">
                </a>
            </div>
        </div>
    </div>
</div>
</section>
<section id="flamp">
<div class="flamp-wrapper wrap">
    <a class="flamp-widget" href="//novosibirsk.flamp.ru/firm/kardiostrazh_ooo_nauchno_proizvodstvennaya_kompaniya-70000001029842003" data-flamp-widget-type="responsive-new" data-flamp-widget-id="70000001029842003" data-flamp-widget-width="100%" data-flamp-widget-count="1"> Отзывы о нас на Флампе</a> 
    <!-- <script>
    7
</script>
--> 
</div>
</section>
<section id="form-mob" class="wrap">
<form action="order.php" method="post">
<div class="mob-form-wr">
    <div class="form-wrap">
        <div class="form-wr sc-content">
            <h4>Контакты</h4>
            <div class="form-box sc-content">
                <p>Имя</p>
                <input type="text" name="name" placeholder="Имя">
            </div>
            <div class="form-box sc-content">
                <p>Телефон для связи</p>
                <input type="text" name="phone" placeholder="+7 (999) 999 99 99">
            </div>
        </div>
        <div class="form-wr sc-content">
            <h4>Характеристики</h4>
            <div class="form-box sc-content">
                <p>Вид материала</p>
                <select name="type" id="">
                    <option>Полиуретановый пластик</option>
                    <option>Полиуретановая резина</option>
                    <option>Пенополиуретан</option>
                    <option>Силикон</option>
                </select>
            </div>
            <div class="form-box sc-content">
                <p>Кол-во, шт</p>
                <input type="text" name="count">
            </div>
        </div>
    </div>
    <div class="btn-form-box">
        <input type="submit" value="Отправить заявку" class="but">
        <label>Отправляя форму, я даю свое согласие на обработку <a href="policy.php" target="_blank" data-fancybox data-type="ajax" data-filter=".policy" data-src="policy.php">персональных данных</a></label>
    </div>
</div>
</form>
</section>
<section id="th">
<div class="contact">
<div class="on map">
    <div class="map-wr">
        <div id="map" style="width:100%;height:100%"></div>
    </div>
    <div class="contact-wr sc-content">
        <h3>Контакты</h3>
        <div class="cont-box sc-content">
            <p class="title">Адрес:</p>
            <p class="text">Новосибирск, ул.Мира 62/1, офис 303</p>
        </div>
        <div class="cont-box sc-content">
            <p class="title">График работы:</p>
            <p class="text">ПН-ПТ с 9:00 до 18:00</p>
        </div>
        <div class="cont-box sc-content">
            <p class="title">Телефон:</p>
            <p class="text"><a href="tel:+79139172791">8 (913) 917-27‒91</a></p>
        </div>
        <div class="cont-box sc-content">
            <p class="title">Почта:</p>
            <p class="text"><a href="mailto:Filborfil@mail.ru">Filborfil@mail.ru</a></p>
        </div>
    </div>
</div>
</div>
</section>
<footer>
<div class="foot">
<div class="wrap">
    <div class="on">
        <div class="lf sc-content">
            <img src="img/logo.png">
        </div>
        <div class="cn sc-content">
            <ul class="footer-menu">
                <li> <a href="#main" class="go">Главная</a> </li>
                <li> <a href="#advantages" class="go">Наши приемущества</a> </li>
                <li> <a href="#on" class="go">Материалы</a> </li>
            </ul>
            <ul class="footer-menu">
                <li> <a href="#tw" class="go">Примеры работ</a> </li>
                <li> <a href="#map" class="go">Контакты</a> </li>
                <li> <a href="policy.php" target="_blank" data-fancybox data-type="ajax" data-filter=".policy" data-src="policy.php">Политика кондифициальности</a> </li>
            </ul>
        </div>
        <div class="rt">
            <div class="r sc-content">
                <p><a href="tel:+79139172791">8 (913) 917-27‒91</a></p>
                <p class="adr">Новосибирск, ул.Мира 62/1, офис 303</p>
            </div>
        </div>
        <!-- <p>
        Сделано в <a href="//sibrix.ru" target="_blank">Сибрикс</a> 
    </p>
    --> 
</div>
<div class="on footer-sib">
    <div class="lf">
        <p>
            &copy; 
            <?php echo date('Y').' '. $_SERVER['HTTP_HOST'] ?>
            </p>
        </div>
        <div class="rt">
            <p>Сделано в <a href="//sibrix.ru" target="_blank">Сибрикс</a></p>
        </div>
    </div>
</div>
</div>
</footer>
<div class="mess">
<div class="a sc-content">
<a href="//wa.me/79139172791" target="_blank">
    <img src="img/ws.png">
</a>
</div>
</div>
<div class="modal" style="display:none">
<div class="on">
<div class="a sc-content">
    <h4>Оставить заявку</h4>
    <p>Заполните поля ниже, мы свяжемся с Вами в течение 5 минут</p>
</div>
<form action="order.php" method="post">
    <input type="text" name="name" placeholder="Имя">
    <input type="text" name="phone" placeholder="+7 (999) 999 99 99">
    <input type="text" name="ord" style="display:none">
    <input type="text" name="mail">
    <label>Отправляя форму, я даю свое согласие на обработку <a href="policy.php" target="_blank" data-fancybox data-type="ajax" data-filter=".policy" data-src="policy.php">персональных данных</a></label>
    <input type="submit" value="Отправить заявку" class="but">
</form>
</div>
</div>
</div>
<script src="js/framework.js" type="text/javascript"></script>
</body>
</html>